const esbuild = require("esbuild");

esbuild
  .build({
    entryPoints: [__dirname + "/src/main.ts"],
    outdir: "dist",
    platform: "node",
    bundle: true,
    sourcemap: true,
    minify: false,
    color: false,
  })
  .catch(() => {
    console.log("COMPILATION ERROR");
    process.exit(1);
  });
