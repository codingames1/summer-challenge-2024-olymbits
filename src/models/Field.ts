export class Field {
    public positions: Array<string>;

    private jumpSymbol = '#';
    private runSymbol = '.';

    constructor(positions: Array<string>) {
        this.positions = positions;
    }

    public nextPositionIsAvailable(position: number): boolean {
        return this.positions[position + 1] === this.runSymbol;
    }

    public nextTwoPositionAreAvailable(position: number): boolean {
        return this.positions[position + 1] === this.runSymbol && this.positions[position + 2] === this.runSymbol;
    }

    public nextThreePositionAreAvailable(position: number): boolean {
        return this.positions[position + 1] === this.runSymbol && this.positions[position + 2] === this.runSymbol && this.positions[position + 3] === this.runSymbol;
    }

}
