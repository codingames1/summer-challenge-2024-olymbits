import {debug} from "../../services/debug";
import {Runner} from "../Runner";
import {Field} from "../Field";

export class Strat1 {

    private readonly runners: Array<Runner>;
    private readonly fields: Array<Field>;

    constructor(runners: Array<Runner>, fields: Array<Field>) {
        this.runners = runners;
        this.fields = fields;
    }

    nextAction(): string {
        let nextThreePositionAreAvailable: boolean = true;
        for (let i = 0; i < this.fields.length; i++) {
            if (!this.fields[i].nextThreePositionAreAvailable(this.runners[i].position)) {
                nextThreePositionAreAvailable = false;
            }
        }
        let action = nextThreePositionAreAvailable ? "RIGHT" : "UP";
        let nextTwoPositionAreAvailable: boolean = true;
        if(action === "UP") {
            for (let i = 0; i < this.fields.length; i++) {
                if (!this.fields[i].nextTwoPositionAreAvailable(this.runners[i].position)) {
                    nextTwoPositionAreAvailable = false;
                }
            }
            action = nextTwoPositionAreAvailable ? "DOWN" : "UP";
        }
        let nextPositionIsAvailable: boolean = true;
        if(action === "UP") {
            for (let i = 0; i < this.fields.length; i++) {
                if (!this.fields[i].nextPositionIsAvailable(this.runners[i].position)) {
                    nextPositionIsAvailable = false;
                }
            }
            action = nextPositionIsAvailable ? "LEFT" : "UP";
        }

        debug('-----');
        debug(action);
        debug('-----');
        return action;
    }
}
