import {Strat1} from "./Strategies/Strat1";
import {Runner} from "./Runner";
import {Field} from "./Field";

export class Game {

    private runners: Array<Runner> = [];
    private fields: Array<Field> = [];

    public resetRunners(): void {
        this.runners = [];
    }

    public resetFields(): void {
        this.fields = [];
    }

    public addPosition(runner: Runner): void {
        this.runners.push(runner);
    }

    public addField(field: string): void {
        this.fields.push(new Field(field.split('')));
    }

    public nextAction(): string {
        const start = new Strat1(this.runners, this.fields);
        this.resetFields();
        this.resetRunners();
        return start.nextAction();
    }
}
