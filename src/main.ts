/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 *
 */
import {Game} from "./models/Game";
import {Runner} from "./models/Runner";
import {debug} from "./services/debug";

// @ts-ignore
const playerIdx: number = parseInt(readline(), 10);
// @ts-ignore
const nbGames: number = parseInt(readline(), 10);

const game = new Game();

// game loop
while (true) {
    for (let i = 0; i < 3; i++) {
        // @ts-ignore
        const scoreInfo: string = readline();
    }
    for (let i = 0; i < nbGames; i++) {
        // @ts-ignore
        let inputs: string[] = readline().split(' ');
        const gpu: string = inputs[0];
        const reg0: number = parseInt(inputs[1], 10);
        const reg1: number = parseInt(inputs[2], 10);
        const reg2: number = parseInt(inputs[3], 10);
        const reg3: number = parseInt(inputs[4], 10);
        const reg4: number = parseInt(inputs[5], 10);
        const reg5: number = parseInt(inputs[6], 10);
        const reg6: number = parseInt(inputs[7], 10);
        debug(gpu);
        game.addPosition(new Runner(reg0));
        game.addField(gpu);
    }

    /*
     * Write an action using console.log()
     * To debug: console.error('Debug messages...');
     */

    console.log(game.nextAction());
}
